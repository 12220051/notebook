package route

import (
	"NoteBook/controller"
	"net/http"

	"github.com/gorilla/mux"
)

func Initialize() {
	router := mux.NewRouter()

	router.HandleFunc("/signup",controller.SignupHandler).Methods("POST")
	router.HandleFunc("/login",controller.LoginHandler).Methods("POST")
	// router.HandleFunc("/user/{email}",controller.GetUser).Methods("GET")

	router.HandleFunc("/profile/{email}",controller.ProfileHandler).Methods("GET")
	router.HandleFunc("/book", controller.CreateBookHandler).Methods("POST")
	router.HandleFunc("/book/{bid}", controller.GetBookHandler).Methods("GET")
	router.HandleFunc("/book/{bid}", controller.UpdateTitleHandler).Methods("PUT")
	router.HandleFunc("/books/{bid}", controller.UpdatePageHandler).Methods("PUT")
	router.HandleFunc("/book/{bid}", controller.DeleteBookHandler).Methods("DELETE")
	router.HandleFunc("/books", controller.GetAllBooksHandeler).Methods("GET")



	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)
	http.ListenAndServe(":8080",router)
}