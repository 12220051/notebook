package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSignup(t *testing.T){
	url := "http://localhost:8080/signup"
	var data = []byte(`{"fname":"Pema","lname":"Wangchuk","email":"pema@gmail.com","password":"pass"}`)
	r,err := http.NewRequest("POST",url,bytes.NewBuffer(data))
	if err != nil{
		panic(err)
	}
	r.Header.Set("Content-type","application/json")
	client := &http.Client{}
	resp, cErr := client.Do(r)
	if cErr != nil{
		panic(cErr)
	}
	defer resp.Body.Close()
	body,rErr := io.ReadAll(resp.Body)
	if rErr != nil{
		panic(rErr)
	}
	assert.Equal(t,http.StatusCreated,resp.StatusCode)

	assert.JSONEq(t,`{"message":"added successfully"}`,string(body))
}
func TestLogin(t *testing.T) {
	url := "http://localhost:8080/login"
	var data = []byte(`{"email":"pema@gmail.com","password":"pass"}`)
	r, err := http.NewRequest("POST",url,bytes.NewBuffer(data))
	if err != nil {
		panic(err)
	}
	r.Header.Set("Content-type","application/json")
	client := &http.Client{}
	resp, Cerr := client.Do(r)
	if Cerr != nil{
		panic(Cerr)
	}
	defer resp.Body.Close()
	body,gErr := io.ReadAll(resp.Body)
	if gErr != nil{
		panic(gErr)
	}
	assert.Equal(t,http.StatusAccepted,resp.StatusCode)
	assert.JSONEq(t,`{"message":"successful"}`,string(body))
	assert.Len(t,resp.Cookies(),1,"Expected one cookie")
	cookie := resp.Cookies()[0]
	assert.Equal(t,"user-cookie",cookie.Name)
	assert.Equal(t,"#@Furpa77",cookie.Value)
}