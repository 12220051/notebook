const fontSelect = document.getElementById('fontSelect');
const textarea = document.getElementById('myTextarea');
const fontSizeButton = document.getElementById('fontSizeButton');

fontSizeButton.addEventListener('click', () => {
  const newFontSize = prompt('Enter a font size (in pixels):');
  if (newFontSize) {
    textarea.style.fontSize = `${newFontSize}px`;
  }
});

fontSelect.addEventListener('change', () => {
  textarea.style.fontFamily = fontSelect.value;
});