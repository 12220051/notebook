// const content = document.querySelector(".contents")
// const addBook = document.querySelector(".books")
// const body = document.querySelector(".container");

// function popup() {
//     addBook.classList.remove("hidden") 
//     body.classList.add("blur")
// }
// function getbook() {
//     body.classList.remove("blur")

//     titleOfTheBook = document.querySelector("#gettitle").value;
//     document.querySelector("#gettitle").value = "";
//     const newBook = document.createElement('div');
//     newBook.classList.add("newnotebook", "bg-div")

//     // enter.onclick = intoPage

//     const editIcon = document.createElement("img")
//     editIcon.src = "images/edit.png"
//     editIcon.onclick = editBookTitle
//     editIcon.classList.add("edit")
   
//     const innerDiv = document.createElement("div");
//     innerDiv.classList.add("title-of-the-book");
//     innerDiv.innerHTML = titleOfTheBook
//     newBook.appendChild(innerDiv);  
//     innerDiv.appendChild(editIcon)


//     const delIcon = document.createElement("img")
//     delIcon.src = "images/dustbin.png"
//     delIcon.onclick = deleteBook
//     delIcon.classList.add("dustbin")    
   
//     newBook.appendChild(delIcon)
//     const entering= document.createElement('div')
//     entering.classList.add('enter')
//     entering.innerHTML = `Enter`
//     entering.onclick = toNewPage
//     newBook.appendChild(entering)

//     content.appendChild(newBook);
//     addBook.classList.add("hidden")
// }
// function back() {
//     document.querySelector("#gettitle").value = "";
//     body.classList.remove("blur")
//     addBook.classList.add("hidden")
// }
// function deleteBook(event) {
//     const delIcon = event.target;
//     const thisbook = delIcon.closest(".newnotebook");
//     const confirmed = confirm("Are you sure you want to delete this book?");
//     if (confirmed) {
//         thisbook.remove();
//     }
// }
// function editBookTitle(event) {
//     const editIcon = event.target
//     const thisbook = editIcon.closest(".newnotebook");
//     const oldtitle = thisbook.querySelector(".title-of-the-book").textContent
//     secondPopup(oldtitle)    
// }
// function secondPopup(title)  {
//     addBook.classList.remove("hidden")
//     document.querySelector("#name").classList.add("hidden")
//     document.querySelector("#rename").classList.remove("hidden")
//     document.querySelector("#newTitle").classList.remove("hidden")
//     document.querySelector("#oldTitle").classList.add("hidden")
//     const inputbar = document.querySelector("#gettitle")
//     inputbar.value = title
//     body.classList.add("blur")
// }
// function toNewPage() {
//     window.location.href = "page.html"
// }

function showMyProfile(data){
    newProfileInof(data)
}

function newProfileInof(user){
    document.getElementById("image").src = user.profileP
    document.getElementById("images").src = user.profileP
    document.getElementById("fname").innerHTML = user.fname
    document.getElementById("lname").innerHTML = user.lname
    document.getElementById("email1").innerHTML = user.email
}
const content = document.querySelector(".contents")
const addBook = document.querySelector(".books")
const body = document.querySelector(".container");

function popup() {
    addBook.classList.remove("hidden") 
    body.classList.add("blur")
}
function getbook() {
    var titleOfTheBook = dataOfBook()
    var bid = titleOfTheBook.BookID

    fetch('/book', {
        method: "POST",
        body: JSON.stringify(titleOfTheBook),
        headers: {"Content-type":"application/json; charset=UTF-8"}
    }).then(res1 => {
        if (res1.ok) {
            fetch('/book/'+bid)
            .then(res2 => res2.text())
            .then(titleOfTheBook => showBook(titleOfTheBook))
        } else {
            throw new Error(res1.statusText)
        }

    }).catch(e => {
        alert("This ID is not available")
        stay()
    })
    function showBook(titleOfTheBook) {

        const bok = JSON.parse(titleOfTheBook)
        frontEndBook(bok)
    } 
}
function dataOfBook() {
    var titleOfTheBook = {
        BookID : parseInt(document.querySelector("#geid").value),
        BookName : document.querySelector("#gettitle").value
    }
    return titleOfTheBook
}
function back() {
    document.querySelector("#gettitle").value = "";
    body.classList.remove("blur")
    addBook.classList.add("hidden")
}
function deleteBook(event) {
    const bookElement = event.target.closest(".newnotebook");
    const bid = bookElement.dataset.BookID; // assuming you've set a "data-book-id" attribute on the book element
    const check = confirm("are you sure you want to delte?")
    
    if (check) {
        fetch(`/book/`+bid, {
            method: "DELETE",
          })
            .then((response) => {
              if (response.ok) {
                  bookElement.remove(); // remove the book element from the page
                
              } else {
                throw new Error("Failed to delete book");
              }
            })
            .catch((error) => {
              console.error(error);
              alert("Failed to delete book");
            });
    }
}
  
function editBookTitle(event) {
    const editIcon = event.target
    const thisbook = editIcon.closest(".newnotebook");
    const oldtitle = thisbook.querySelector(".title-of-the-book").textContent
    secondPopup(oldtitle)    
}
function secondPopup(title)  {
    addBook.classList.remove("hidden")
    document.querySelector("#name").classList.add("hidden")
    document.querySelector("#rename").classList.remove("hidden")
    document.querySelector("#newTitle").classList.remove("hidden")
    document.querySelector("#oldTitle").classList.add("hidden")
    const inputbar = document.querySelector("#gettitle")
    inputbar.value = title
    body.classList.add("blur")
}
function toNewPage() {
    window.location.href = "page.html"
}
function backToPage() {
    addBook.classList.add("hidden")
    body.classList.remove("blur")
    document.querySelector("#gettitle").value = "";
    document.querySelector("#geid").value = "";
}
function stay() {
    addBook.classList.remove("hidden")
    body.classList.add("blur")
    document.querySelector("#geid").value = "";
}
function dataOfBook() {
    var titleOfTheBook = {
        BookID : parseInt(document.querySelector("#geid").value),
        BookName : document.querySelector("#gettitle").value
    }
    return titleOfTheBook
}
window.onload = function() {
    alert("hi")
    const email = JSON.parse(localStorage.getItem("userEmail"))
    showMyProfile(email)
    fetch('/books')
    .then(response => response.text())
    .then(data => showBooks(data))
}
function showBooks(data) {
    const bok = JSON.parse(data)
    bok.forEach(bok => {
        frontEndBook(bok)
    });
}
function frontEndBook(bok) {
    const newBook = document.createElement('div');
    newBook.classList.add("newnotebook", "bg-div");

    // set the BookID as a data attribute
    newBook.dataset.BookID = bok.BookID;

    const editIcon = document.createElement("img");
    editIcon.src = "images/edit.png";
    editIcon.onclick = editBookTitle;
    editIcon.classList.add("edit");
   
    const innerDiv = document.createElement("div");
    innerDiv.classList.add("title-of-the-book");
    innerDiv.innerHTML = bok.BookName;
    newBook.appendChild(innerDiv);  
    innerDiv.appendChild(editIcon);
  
    const delIcon = document.createElement("img");
    delIcon.src = "images/dustbin.png";
    delIcon.onclick = deleteBook;
    delIcon.classList.add("dustbin");    
   
    newBook.appendChild(delIcon);
    const entering= document.createElement('div');
    entering.classList.add('enter');
    entering.innerHTML = `Enter`;
    entering.onclick = toNewPage;
    newBook.appendChild(entering);

    backToPage();

    return (content.appendChild(newBook));
}

function showProfile() {
    body.classList.add("blur")
    document.querySelector(".pro-body").classList.remove("hidden")
}

function editProfile() {
    document.querySelector(".before-edit").classList.add("hidden")
    document.querySelector(".after-edit").classList.remove("hidden")
}
function saveProfile() {
    document.querySelector(".before-edit").classList.remove("hidden")
    document.querySelector(".after-edit").classList.add("hidden")

    const readEditedProfile = getEditedProfile()

    document.querySelector(".u-fname>span").innerHTML = readEditedProfile.fname
    document.querySelector(".u-lname>span").innerHTML = readEditedProfile.lname
    document.querySelector(".u-email>span").innerHTML = readEditedProfile.email

}
function getEditedProfile() {
    const newProfile = {
        fname: document.getElementById("edit-fname").value,
        lname: document.getElementById("edit-lname").value,
        email: document.getElementById("edit-email").value
    }   
    return newProfile 
}
function backToPage() {
    body.classList.remove("blur")
    document.querySelector(".pro-body").classList.add("hidden")
}

function getLogout() {
    window.location.href = "index.html"
}








// window.onload = function() {
//     var iframe = document.getElementById("frame");
//     var otherPageDocument = iframe.contentDocument;
//     document.querySelector(".u-fname>span").innerHTML = otherPageDocument.querySelector("#fname").value;
//     document.querySelector(".u-fname>span").innerHTML = otherPageDocument.querySelector("lname").value;
//     document.querySelector(".u-email>span").innerHTML = otherPageDocument.querySelector("#email").value;  
// }
