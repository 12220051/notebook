function CreateUser() {
    var pic = GetProfilePicture()
    console.log(pic)
    var data = {
        fname: document.getElementById("fname").value,
        lname: document.getElementById("lname").value,
        email: document.getElementById("email").value,
        password: document.getElementById("password").value,
        pw2 : document.getElementById("pw2").value,
        profileP : pic

    }
    if (data.fname == "" || data.email == "" || data.password == ""){
        alert("Enter the information completely")
        return 
    }
    if (data.password != data.pw2){
        alert("Password is not matching")
        return 
    }
    fetch("/signup",{
        method: "POST",
        body: JSON.stringify(data),
        headers:{"content-type":"application/json; charset-utf 8"}
    }).then(response => {
       if(response.status == 201){
            window.location.href = "index.html"
       }else if (response.status == 406){
            alert("The email is already registered with us, try logging in")
       }else{
            throw new Error(response.statusText)
       }
    }).catch(e =>{
        alert(e)
    })
}

document.querySelector("#profilePic").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("picture", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})
function GetProfilePicture() {
    alert(localStorage.getItem("picture"))
    const proimg = localStorage.getItem("picture")
    console.log(proimg)
    return proimg
}