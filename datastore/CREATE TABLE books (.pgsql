CREATE TABLE books (
    BookID int NOT NULL PRIMARY KEY,
    BookName VARCHAR(20) NOT NULL,
    Page TEXT
)