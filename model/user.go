package model

import (
	postgres "NoteBook/datastore"
)
type User struct {
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	ProfilePicture string `json:"profileP"`
}


func (u *User) CreateUser(a []byte) error {
	const queryCreateUser = "INSERT INTO users (FirstName, LastName,Email,Password,ProfilePicture) VALUES($1,$2,$3,$4,$5);"
	_,err := postgres.Db.Exec(queryCreateUser,u.FirstName,u.LastName,u.Email,a,u.ProfilePicture)
	return err
}

func (u *User) Check() ([]byte,error){
	var dbHash []byte
	const queryCheck = "SELECT * FROM users WHERE Email = $1;"
	err := postgres.Db.QueryRow(queryCheck,u.Email).Scan(&u.FirstName,&u.LastName,&u.Email,&dbHash,&u.ProfilePicture)
	return dbHash, err
}

func (u *User) GetProfile(email string)error {
	const queryGetProfile = "Select * from users where email = $1;"
	err := postgres.Db.QueryRow(queryGetProfile,email).Scan(&u.FirstName,&u.LastName,&u.Email,&u.Password,&u.ProfilePicture)
	return err
}
